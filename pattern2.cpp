#include<bits/stdc++.h>
using namespace std;

int main()
{
    int rows,j;
    cout<<"Enter a number of rows: ";
    cin>>rows;
    for(int i = 1; i<=rows; i++)
    {
        for( j = rows; j>=i; j--)
        {
            cout<<" ";
        }
            if(i==1)
            {
                for(j = 1; j<=i; j++)
                {
                    cout<<"* ";
                }
            }
            else if(i==rows)
            {
                for(j = 1; j<rows*2; j++)
                {
                    cout<<"*";
                }
            }
            else
            {
                cout<<"*";
                for(j = 1; j<=2*i-3; j++)
                {
                    cout<<" ";
                }
                cout<<"*";
            }
            cout<<endl;
    }
    return 0;
}
